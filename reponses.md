# TP Realworld

##6.3

###6.3.1

SHA1: 7a2e472d9d9e77ad7de8f4ace9e09d7597ee60f7

###6.3.2

URL node-8-debian: https://github.com/mkenney/docker-npm/blob/master/node-8-debian/Dockerfile

Volume déclaré: /src

docker image pull mkenney/npm:node-8-debian

id de l'image: 9af19a7f4e2c

###6.3.3

commande construction du front: docker container run --rm -i -v $(pwd):/src mkenney/npm:node-8-debian npm install

###6.3.4

docker run --rm -ti -v $(pwd):/src -p 8000:8080 mkenney/npm:node-8-debian npm run dev

##6.4

###6.4.1

SHA1: eef3d052fe83dc688c0eb3dab97876d714a2d14d

###6.4.2

docker image pull gradle:4.7.0-jdk8-alpine
id image: f438b7d58d0a5296535fa98519056365bde40691e3c3b0c2a64950abcf336b05
volume: /home/gradle/.gradle

###6.4.3

docker volume create gradle-home

lister les volumes: docker volume ls

résultat:

DRIVER              VOLUME NAME
local               0113b81d9021b9a5848a043c70a6f40deec6460694a985f9f710fe6cc9fb1dff
local               37a3a645bc013c11e428cfbb5d390d534f85a9c87054ef151815fa1e58d957a3
local               c718491fd2b3eb963c50e1e5112f16870126e7f16f01571f82d819cbe44e1cc3
local               gradle-home

###6.4.4

docker container run --rm -ti -v $(pwd):/src -v gradle-home:/home/gradle/.gradle -w /src gradle:4.7.0-jdk8-alpine gradle build

###6.4.5

docker container run --rm -ti -v $(pwd):/src -v gradle-home:/home/gradle/.gradle -p 8001:8080 -w /src gradle:4.7.0-jdk8-alpine gradle bootRun

##6.5

###6.5.1

reponse: {"articles":[],"articlesCount":0}
